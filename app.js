const express = require("express");
const res = require("express/lib/response");
const app = express();
const port = 3000;
const { loadCar, findCar } = require("./utils/cars");
//guunakan ejs
app.set("view engine", "ejs");

// statis file
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/images", express.static(__dirname + "public/images"));
app.use("/img", express.static(__dirname + "public/img"));
app.use("/js", express.static(__dirname + "public/js"));
app.use("/scss", express.static(__dirname + "public/scss"));
app.use("/vendor", express.static(__dirname + "public/vendor"));

app.get("/", (req, res) => {
  //   res.send("hello world!");
  //   res.sendFile("./index.html", { root: __dirname });
  const cars = loadCar();

  res.render("index", {
    cars,
  });
});

// halaman form tambah data contact
app.get("/car", (req, res) => {
  res.render("cars");
});

app.get("/car/:nama", (req, res) => {
  const car = findCar(req.params.nama);

  res.render("detail", {
    car,
  });
});

//proses data contact
app.post("/car", (req, res) => {
  res.send(req.body);
});

app.use("/", (req, res) => {
  res.status(404);
  res.send("<h1>404</h1>");
});

app.listen(port, () => {
  console.log(`challange5 app listening at http://localhost:${port}`);
});
