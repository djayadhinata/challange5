const fs = require("fs");

//membuat folder data jika belum ada
const dirPath = "./data";
if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath);
}

// membuat file cars.json jika belum ada
const dataPath = "./data/cars.json";
if (!fs.existsSync(dataPath)) {
  fs.writeFileSync(dataPath, "[]", "utf-8");
}

//ambil semua data di cars.json
const loadCar = () => {
  const fileBuffer = fs.readFileSync("data/cars.json", "utf-8");
  const cars = JSON.parse(fileBuffer);
  return cars;
};

//cari 1 data di car.json
const findCar = (nama) => {
  const cars = loadCar();
  const car = cars.find((car) => car.nama.toLowerCase() === nama.toLowerCase());
  return car;
};

module.exports = { loadCar, findCar };
